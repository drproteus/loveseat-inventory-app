angular.module('starter')

.service('AuthService', function($q, $http, USER_ROLES, ADMINS, SERVER_ADDRESS) {
  var LOCAL_TOKEN_KEY = 'yourTokenKey';
  var username = '';
  var isAuthenticated = false;
  var role = '';
  var authToken;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }

  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }

  function useCredentials(token) {
    username = token.split('%')[0];
    isAuthenticated = true;
    authToken = token;

    if (ADMINS.indexOf(username) !== -1) {
      role = USER_ROLES.admin;
    } else {
      role = USER_ROLES.standard;
    }

    //$http.defaults.headers.common['X-AUTH-Token'] = token;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    username = '';
    isAuthenticated = false;
    //$http.defaults.headers.common['X-AUTH-Token'] = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }

  var login = function(name, pw) {
    return $q(function(resolve, reject) {
      $http({
        url: SERVER_ADDRESS+'/api/auth/login/',
        method: 'PUT',
        headers: {
          'Authorization': window.btoa('Basic '+name+':'+pw),
        }
      }).success(function(response) {
        if ('Error' in response) {
          console.log(response);
          reject('Login Failed.');
        } else {
          console.log(response);
          storeUserCredentials(name+'%serverToken');
          resolve('Login Successful.');
        }
      }).error(function(response) {
        reject('Login Failed.');
      });
    });
  };

  var logout = function() {
    destroyUserCredentials();
  };

  var isAuthorized = function(authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }
    return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
  };

  loadUserCredentials();

  return {
    login: login,
    logout: logout,
    isAuthorized: isAuthorized,
    isAuthenticated: function() {return isAuthenticated;},
    username: function() {return username;},
    role: function() {return role;},
  };
})

.factory('AuthInterceptor', function($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function(response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
        403: AUTH_EVENTS.notAuthorized,
      }[response.status], response);
      return $q.reject(response);
    }
  };
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
});
