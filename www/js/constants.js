angular.module('starter')

.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
})

.constant('USER_ROLES', {
  admin: 'admin_role',
  standard: 'standard_role'
})

.constant('ADMINS', [
  'jgoritski@gmail.com'
])

.constant('SERVER_ADDRESS', 'http://www.theloveseat.net');
