angular.module('starter')

.controller('AppCtrl', function($scope, $state, $ionicPopup, AuthService, AUTH_EVENTS) {
  $scope.username = AuthService.username();

  $scope.$on(AUTH_EVENTS.notAuthorized, function(event) {
    var alertPopup = $ionicPopup.alert({
      title: 'Unauthorized',
      template: 'You are not allowed to access this resource.'
    });
  });

  $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
    AuthService.logout();
    $state.go('login');
    var alertPopup = $ionicPopup.alert({
      title: 'Session Expired',
      template: 'Please login again.'
    });
  });

  $scope.setCurrentUsername = function(name) {
    $scope.username = name;
  };
})

.controller('DashCtrl', function($scope, $state, AuthService) {
  $scope.logout = function() {
    AuthService.logout();
    $state.go('login');
  }
})

.controller('ItemsCtrl', function($scope, $http, SERVER_ADDRESS) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.items = [];
  $http.get(SERVER_ADDRESS+'/api/item/active/').
      success(function (data) {
          $scope.items = data;
      });
})

.controller('ItemDetailCtrl', function($scope, $stateParams, $http, SERVER_ADDRESS) {
  $scope.item = {};
  $http.get(SERVER_ADDRESS+'/api/item/'+$stateParams.itemId+'/').
      success(function (data) {
          $scope.item = data;
      });
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('LoginCtrl', function($scope, $state, $ionicPopup, AuthService) {
  $scope.data = {};
  $scope.login = function(data) {
    AuthService.login(data.username, data.password).then(function(authenticated) {
      $state.go('tab.dash', {}, {reload: true});
      $scope.setCurrentUsername(data.username);
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'Login Failed',
        template: 'Please check your credentials.',
      });
    });
  };
});

